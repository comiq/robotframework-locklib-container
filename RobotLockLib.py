import time
import os
from threading import Lock

from robot.api import logger

def remote_log_message(message, level, html=False):
  print '{} {}'.format(level, message)

logger.write = remote_log_message

class RobotLockLib(object):

  def __init__(self):
    self._masterlock = Lock()
    self._locks = {}

  def acquire_lock(self, resource, context='_', timeout=360):
    logger.write('Acquire lock for resource {}, by {}'.format(resource, context), "INFO")
    start_time = time.time()
    while time.time() < start_time + int(timeout):
      if self._check_lock(resource, context):
        return True
      logger.write('Waiting resource {} to be relesed by {}, lock level is {}'.format(resource, self._locks[resource][0],self._locks[resource][1]), "INFO")
      time.sleep(0.5)
    raise Exception('Time out waiting lock for resource')

  def _check_lock(self, resource, context):
    with self._masterlock:
      if resource in self._locks and context != self._locks[resource][0]:
        return False
      if resource not in self._locks:
        self._locks[resource] = [context,0]
      self._locks[resource][1]+=1
      return True

  def is_locked(self, resource):
    with self._masterlock:
      if resource in self._locks:
        return True
      else:
        return False

  def release_lock(self, resource, context='_'):
    with self._masterlock:
      if resource not in self._locks:
        return False
      if resource in self._locks and context != self._locks[resource][0]:
        return False
      if self._locks[resource][1] > 0:
        self._locks[resource][1]-=1
        if self._locks[resource][1] == 0:
          del self._locks[resource]
          logger.write('Released lock for resource {}, by {}'.format(resource, context), "INFO")
        return True
      return False

if __name__ == '__main__':
  import sys
  from robotremoteserver import RobotRemoteServer

  RobotRemoteServer(RobotLockLib(), host="0.0.0.0", port=int(os.environ["LIBRARY_PORT"]), allow_stop=False)
