#!/bin/bash

docker-compose stop
docker-compose rm -f
docker-compose up -d locklib
#docker-compose up robotframework_locking_logic
docker-compose up -d robotframework_parallel_locking_1
docker-compose up -d robotframework_parallel_locking_2
docker-compose up -d robotframework_parallel_locking_3
docker-compose logs -f

