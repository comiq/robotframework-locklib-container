*** Settings ***
Library           Remote    http://locklib:8270    WITH NAME    locklib

*** Test Cases ***
Should not be locked in beginning
  ${locked}=    Is Locked    'limited resource'
  Should not be True    ${locked}    'Resource should not be locked'

Should not release lock of unlocked resource
  ${released}=    Release Lock    'limited resource'
  Should not be True    ${released}    'Resource should not be released'

Should acquire lock
  ${locked}=    Acquire lock   'limited resource'    'Main'
  Should be True   ${locked}    'Resource should be locked'

Should not release lock which is not owned
  ${released}=    Release Lock    'limited resource'    'Other'
  Should not be True    ${released}    'Resource should be released'

Should release owned lock
  ${released}=    Release Lock    'limited resource'    'Main'
  Should be True    ${released}    'Resource should be released'

Should not release released lock
  ${released}=    Release Lock    'limited resource'    'Main'
  Should not be True    ${released}    'Resource should not be released'

Should allow acquiring lock by same caller
  ${locked}=    Acquire lock   'limited resource'    'Main'
  Should be True   ${locked}    'Resource should be locked'
  ${locked}=    Acquire lock   'limited resource'    'Main'
  Should be True   ${locked}    'Resource should be locked'
  ${locked}=    Acquire lock   'limited resource'    'Main'
  Should be True   ${locked}    'Resource should be locked'

Should decrease locking by same caller
  ${released}=    Release Lock    'limited resource'    'Main'
  Should be True    ${released}    'Resource should be released'
  ${released}=    Release Lock    'limited resource'    'Main'
  Should be True    ${released}    'Resource should be released'
  ${released}=    Release Lock    'limited resource'    'Main'
  Should be True    ${released}    'Resource should be released'
  ${released}=    Release Lock    'limited resource'    'Main'
  Should not be True    ${released}    'Resource should not be released'

Should lock if acquire lock by different caller
  ${locked}=    Acquire lock   'limited resource'    'Main'    1
  Should be True   ${locked}    'Resource should be locked'
  ${success}=    Run keyword and Return Status    Acquire lock   'limited resource'    'Other'   1
  Should not be True   ${success}    'Acquiring lock should fail'

Should allow locking by different caller after releasing
  ${released}=    Release Lock    'limited resource'    'Main'
  Should be True    ${released}    'Resource should be released'
  ${success}=    Run keyword and Return Status    Acquire lock   'limited resource'    'Other'   1
  Should be True   ${success}    'Acquiring lock should pass'
  ${released}=    Release Lock    'limited resource'    'Other'
  Should be True    ${released}    'Resource should be released'

