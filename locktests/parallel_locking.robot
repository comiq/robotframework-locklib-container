*** Settings ***
Library           Remote    http://locklib:8270    WITH NAME    locklib

*** Variables ***
${resource_id}=  'RESOURCE'

*** Test Cases ***
Acquire Lock to Shared Resource
  ${runner_id}=   Evaluate    uuid.uuid4()    modules=uuid
  Sleep random
  ${locked}=    Acquire lock   ${resource_id}    ${runner_id}    60
  Should be True   ${locked}    'Resource should be locked'
  Sleep random
  Release Lock    ${resource_id}    ${runner_id}

*** Keywords ***
Sleep random
  ${random int}=    Evaluate    random.randint(3, 8)    modules=random
  Log to console   'Sleep for ${random int} seconds'
  Sleep    ${random int}