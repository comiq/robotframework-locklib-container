FROM alpine:3.3

RUN apk update
RUN apk add --no-cache \
  python \
  py-pip \
  py-psutil \
  python-dev \
  gcc

RUN pip install robotframework robotremoteserver

EXPOSE 8270
ENV LIBRARY_PORT=8270

COPY robotlocklib.sh /usr/local/bin/
COPY RobotLockLib.py RobotLockLib.py

RUN chmod ug+x /usr/local/bin/robotlocklib.sh && \
  chmod ug+x RobotLockLib.py

ENTRYPOINT ["/usr/local/bin/robotlocklib.sh"]

